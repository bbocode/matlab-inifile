function test_ini_pvargs
% Test argument parser (ini.pvargs).
TEST.CHAPTER('ini.pvargs')

TEST.SECTION('no unparsed arguments')

TEST.EVAL('testargs = {''Name'', ''myname'', ''Value'', ''myvalue''};')
TEST.EVAL('p = ini.pvargs([], {''Name'', ''Value''});')
TEST.EXPECT('isstruct(+p)')
TEST.EXPECT('isempty(fieldnames(+p))')
TEST.EVAL('[o,u] = +p;')
TEST.EXPECT('iscell(u)')
TEST.EXPECT('isempty(u)')

TEST.EVAL('p = ini.pvargs([], {''Name'', ''Value''}, testargs{:});')
TEST.EVAL('[o,u] = +p;')
TEST.EXPECT('~isempty(fieldnames(o))')
TEST.EXPECT('strcmp(o.Name, ''myname'')')
TEST.EXPECT('iscell(u)')
TEST.EXPECT('isempty(u)')

TEST.EVAL('p = ini.pvargs([], struct(''Name'', ''name'', ''Value'', ''value''));')
TEST.EVAL('[o,u] = +p;')
TEST.EXPECT('~isempty(fieldnames(o))')
TEST.EXPECT('strcmp(o.Name, ''name'')')
TEST.EXPECT('strcmp(o.Value, ''value'')')
TEST.EXPECT('isempty(u)')

TEST.EVAL('p = ini.pvargs([], struct(''Name'', ''name'', ''Value'', ''value''), testargs{:});')
TEST.EVAL('[o,u] = +p;')
TEST.EXPECT('strcmp(o.Name, ''myname'')')
TEST.EXPECT('strcmp(o.Value, ''myvalue'')')
TEST.EXPECT('isempty(u)')

TEST.SECTION('with unparsed arguments')

TEST.EVAL('testargs = {''Name'', ''myname'', ''Value'', ''myvalue'', ''Unparsed'', ''unparsed''};')
TEST.EVAL('p = ini.pvargs([], struct(''Name'', ''name'', ''Value'', ''value''), testargs{:});')
TEST.EVAL('[o,u] = +p;')
TEST.EXPECT('strcmp(o.Name, ''myname'')')
TEST.EXPECT('strcmp(o.Value, ''myvalue'')')
TEST.EXPECT('~isempty(u)')

TEST.EXPECT('o = +p;', 'Throw', 'INI:pvargs:unparsedArguments')