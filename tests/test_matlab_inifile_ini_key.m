function test_matlab_inifile_ini_key
global g_matlab_inifile %#ok<NUSED>
% Test INI keys (ini.key).

rule = test_matlab_inifile_ini_rule();

TEST.CHAPTER('ini.key')

% ========================================================================
TEST.SECTION('Without rules')

TEST.EXPECT('ini.key();', 'Throw', 'INI:nameRequired')
TEST.EXPECT('class(ini.key(''Name'', ''key''));', 'Result', 'ini.key')

TEST.EVAL('key = ini.key(''Name'', ''key'', ''Value'', ''value'');')
TEST.EXPECT('+key', 'Result', 'value');

TEST.SECTION('Methods')

TEST.EVAL('key = ini.key(''Name'', ''key'');')
TEST.EXPECT('isempty(key.get())')
TEST.EXPECT('key.isUnassigned() == true')
TEST.EXPECT('key.isModified() == false')

TEST.EXPECT('key.Value = 10;', 'Throw', 'MATLAB:class:SetProhibited')
TEST.EVAL('key.set(10);')
TEST.EXPECT('key.isModified() == true')
TEST.EXPECT('key.get() == 10')

TEST.EVAL('key.write(1);')
TEST.EXPECT('key.isModified() == false')

TEST.EVAL('key.set(11);')
TEST.EXPECT('key.isModified() == true')
TEST.EXPECT('key.Value == 11')

% ========================================================================
TEST.SECTION('Rules')

rule_global = rule.Children{1};
rule_global_version = rule_global.Children{1}; %#ok<NASGU>

TEST.EVAL('key = ini.key(''Rule'', rule_global_version);')
TEST.EVAL('key.set(11);')
TEST.EXPECT('g_matlab_inifile.callback.reason == ini.reason.SET');
TEST.EXPECT('key.isModified() == true')
TEST.EXPECT('key.get() == 11')
TEST.EXPECT('g_matlab_inifile.callback.reason == ini.reason.GET');
