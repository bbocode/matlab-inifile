function FIXME(varargin)
% Utility to mark code location that needs to be fixed.
    if islogical(varargin{1})
        if varargin{1}
            fprintf(2, varargin{2:end});
        else
            error(varargin{2:end});
        end
    else
        error(varargin{:})
    end
end