function EXPECT(assertion, varargin)
% Test assertion.
%
% USAGE
%   TEST.EXPECT('command', 'PropertyName', propertyvalue, ...)
%
% PROPERTIES
%   Throw 'identifier'              Expect an error to be thrown.
%
    args = struct(varargin{:});
    if ~isfield(args, 'Result')
        args.Result = [];
    end
    
    success = true;
    message = '[Ok]';
    throw = false;
    
    % evaluate
    try 
        if isfield(args, 'Throw') && ~isempty(args.Throw)
            evalin('caller', assertion);
        else
            result = evalin('caller', assertion);
        end
    catch me
        if isfield(args, 'Throw') && ~isempty(args.Throw)
            throw = true;
            
            if islogical(args.Throw) || ~strcmp(me.identifier, args.Throw)
                message = ['[Failed (' me.identifier ')]'];
                success = false;
            else
                message = ['[Ok (' me.identifier ')]'];
            end
        else
            rethrow(me);
        end
    end

    % check results
    if ~isfield(args, 'Throw') || isempty(args.Throw)
        if isempty(args.Result)
            success = result;    
        else
            switch class(args.Result)
                case 'logical'
                    if args.Result
                        assertion = [ assertion ' == true'];
                    else
                        assertion = [ assertion ' == true'];
                    end
                    
                    success = (result == args.Result);

                case 'double'
                    assertion = sprintf('%s == %s', assertion, string(args.Result));
                    success = (result == args.Result);

                case 'char'
                    try 
                        success = strcmp(result, args.Result);
                        assertion = sprintf('%s == ''%s''', assertion, args.Result);
                    catch me 
                        success = false;
                        message = ['[Failed with ''' me.message '''.]'];
                    end
            end
        end
    else
        if islogical(args.Throw) 
            if args.Throw
                error('Internal error: ''Throw'' can be false or the expected reason for an exception.')
            end
        else
            if ~throw
                message = ['[Failed (expected ' args.Throw ')]'];
                success = false;
            end
        end
    end
    
    % output
    if success
        fid = 1;
    else
        fid = 2;
        if message(2:3) == 'Ok' %#ok<STCMP>
            message = '[Failed]';
        end
    end
    
    fprintf(fid, '%-60s %s\n', assertion, message);
end

