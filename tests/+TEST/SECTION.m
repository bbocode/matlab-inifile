function SECTION(varargin)
    fprintf(1, ['\n=== ' varargin{1} '\n'], varargin{2:end});
end

