function EVAL(assertion, varargin)
% Output evaluation.
%
% USAGE
%   TEST.EVAL('command')

    fprintf(1, '## %s\n', assertion);
    evalin('caller', assertion);
end

