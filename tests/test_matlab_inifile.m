function test_matlab_inifile()

[path,mfile] = fileparts(mfilename('fullpath'));

list = dir(fullfile(path, 'test_matlab_inifile_*.m'));
for i = 1:length(list)
    item = list(i);
    
    [~,fun,~] = fileparts(item.name);
    if strcmp(fun, mfile)
        continue
    end
    
    fprintf(1, 'FILE %s\n', fun);
    eval(fun)
end