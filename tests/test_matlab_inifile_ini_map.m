function test_matlab_inifile_ini_map
% Test INI sub-sections (ini.map).

rule = test_matlab_inifile_ini_rule();

TEST.CHAPTER('ini.map')

% ========================================================================
TEST.SECTION('Without rules')

TEST.EXPECT('ini.map()', 'Throw', 'INI:nameRequired')

TEST.EVAL('map = ini.map(''Name'', ''section'', ''Value'', {''sub-section'', struct(''key'', ''keyvalue'')});')
TEST.EXPECT('map.isUnassigned() == false;')
TEST.EVAL('map.reset();')
TEST.EXPECT('map.isUnassigned() == true;')
TEST.EVAL('map.edit(''SubSection'', ''sub-section'', ''Create'', ''true'');') % without rules, there's nothing to edit
TEST.EXPECT('map.isUnassigned() == true;')

% ========================================================================
TEST.SECTION('Rules')

rule_host = rule.Children{3}; %#ok<NASGU>

TEST.EVAL('map = ini.map(''Rule'', rule_host);')
TEST.EXPECT('map.isUnassigned() == true;')
TEST.EXPECT('map.isModified() == false;')

TEST.EVAL('map.set(struct(''user'', ''me''), ''SubSection'', ''bitbucket.org'');')
TEST.EXPECT('map.isModified() == true;')
