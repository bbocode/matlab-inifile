function test_matlab_inifile_ini_section
% Test INI sections (ini.section).

rule = test_matlab_inifile_ini_rule();

TEST.CHAPTER('ini.section')

% ========================================================================
TEST.SECTION('Without rules')

TEST.EXPECT('ini.section()', 'Throw', 'INI:nameRequired')

TEST.EVAL('sec = ini.section(''Name'', ''section'', ''Value'', struct(''key'', ''keyvalue''));')
TEST.EXPECT('sec.isUnassigned() == false;')

TEST.EVAL('data = +sec;')
TEST.EXPECT('isfield(data, ''key'')');
TEST.EXPECT('strcmp(data.key, ''keyvalue'')');
TEST.EXPECT('sec.get(''Key'', ''invalidkey'')', 'Throw', 'INI:section:noSuchKey');
TEST.EXPECT('+sec.get(''Key'', ''key'')', 'Result', 'keyvalue');

TEST.EXPECT('sec.set(''set_key_by_name'', ''Key'', ''key'');', 'Throw', false);
TEST.EXPECT('+sec.get(''Key'', ''key'')', 'Result', 'set_key_by_name');

TEST.EXPECT('sec.set(struct(''key'', ''set_key_by_struct''));', 'Throw', false);
TEST.EXPECT('+sec.get(''Key'', ''key'')', 'Result', 'set_key_by_struct');

% ========================================================================
TEST.SECTION('Rules')

rule_global = rule.Children{1}; %#ok<NASGU>

TEST.EVAL('sec = ini.section(''Rule'', rule_global);')
TEST.EXPECT('sec.isUnassigned() == false;')
