# INI file handling for Matlab� #

This package provides a bridge between [INI files (Wikipedia)](https://en.wikipedia.org/wiki/INI_file) and Matlab�. 

For further details, see 

	help INI

or the example_matlab_inifile.m file in the tests/ directory.

## License ##

This software package as a whole is published under the
[GNU General Public License Version 3](https://www.gnu.org/licenses/gpl-3.0-standalone.html).

	Copyright � 2017 Research center caesar, Bonn, Germany

	Authors:
	J�rgen "George" Sawinski

