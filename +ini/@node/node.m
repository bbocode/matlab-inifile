classdef node
    % Node types.
    enumeration
        UNDEFINED,      % Initial, undefined node type.
        
        KEY,            % Key (<a href="https://en.wikipedia.org/wiki/INI_file#Keys_.28properties.29">key or property</a>)
        SECTION,        % Section (FIXME).
        SUBSECTION,     % Sub-section (FIXME).
        MAP,            % Sub-section map (FIXME).
        CONFIG,         % Configuration (@INI).
        
        RULESET         % A rule-set.
    end
end

