classdef key < handle
    % Representation of an INI key.
    %
    % The basic element contained in an INI file is the 
    % <a href="https://en.wikipedia.org/wiki/INI_file#Keys_.28properties.29">key or property</a>.
    % 
    % Value types supported in this implementation are
    % - strings (char)
    % - numbers (double)
    % - boolean (logical)
    % including arrays of these types.
    %
    % USAGE
    %   object = ini.key('PropertyName', propertyvalue, ...)
    %
    % PROPERTIES
    %   Name            Name of the key (required).
    %   Value           Initial key value.
    %   Rule            A rule defining type etal. (see ini.rule).
    %
    % SEE ALSO
    %   <a href="matlab:help INI">INI</a>
    
    properties(SetAccess={?ini.pvargs,?ini.rule})
        Name                % Name of this key.
        Value               % Value of this key.
    end

    properties(Hidden, SetAccess={?ini.pvargs,?ini.rule})
        SavedValue  = []    % Previous value.
        Parent              % Parent section.
        Rule                % Rule for this key.
    end
    
    properties(Constant,Hidden)
        Node = ini.node.KEY
    end

    %%% Constructor and de-referencing.
    methods
        function self = key(varargin)
            % Constructor.
            
            % parser arguments
            [~,value] = +ini.pvargs(self, struct(...
                'Name', [], ...
                'Rule', ini.rule, ...
                'Parent', []), varargin{:});
            [value] = +ini.pvargs([], struct(...
                'Value', self.Rule.Default), value{:});

            % sanity checks
            if self.Rule.isEmpty()
                if isempty(self.Name)
                    error('INI:nameRequired', 'Sections or keys require a name (''Name'' property).');
                end
            else
                if ~isempty(self.Name) && ~strcmp(self.Name, self.Rule.Tag)
                    error('INI:nameOverdefined', 'Property ''Name'' over-defines section or key (as it is provided by the Rule).');
                end
                
                self.Name = self.Rule.Tag;
            end
            
            % initialize
            value = value.Value;
            self.Rule.assign(self, value, 'Reason', ini.reason.INIT);
            self.markSaved();
        end
        
        function res = uplus(self)
            % De-reference value of this key.
            res = self.get();
        end
    end
    
    %%% Methods
    methods
        function res = isUnassigned(self)
            % Check, if value is unassigned (empty).
            
            res = isempty(self.Value);
        end
        
        function res = isModified(self)
            % Check if property value has changed.
            
            if isempty(self.Value) ~= isempty(self.SavedValue)
                res = true;
            elseif isempty(self.Value) && isempty(self.SavedValue)
                res = false;
            else
                switch class(self.Value)
                    case 'char'
                        res = ~strcmp(self.Value, self.SavedValue);

                    case { 'logical', 'double' }
                        res = (self.Value ~= self.SavedValue);

                    otherwise
                        error('Internal error.')
                end
            end
        end

        function self = markSaved(self)
            % Set key to unmodified state.
            
            self.SavedValue = self.Value;
        end
        
        function res = get(self, varargin)
            % Get value.
            %
            % USAGE
            %     result = object.get('PropertyName', propertyvalue, ...)
            %
            % PROPERTIES
            %     Default <value>               Expected default value (if empty).

            res = self.Rule.retrieve(self, varargin{:}).Value;
        end
        
        function set(self, value, varargin)
            % Set value.
            %
            % USAGE
            %     object.set(value, 'PropertyName', propertyvalue, ...)
            %
            % PROPERTIES
            %     Reason <reason>               The reason for set operation (see ini.reason).
            
            args = +ini.pvargs([], struct(...
                'Reason', ini.reason.SET, ...
                'Default', [], ...  % ignored
                'Create', false ... % ignored
            ), varargin{:});
            
            self.Rule.assign(self, value, 'Reason', args.Reason);
        end
        
        function self = reset(self)
            % Reset to default.
            %
            % USAGE
            %     object.reset()
            
            self.Rule.assign(self, self.Rule.Default, 'Reason', ini.reason.RESET);
        end
        
        function self = edit(self, varargin)
            % Edit value.
            %
            % USAGE
            %     object.set('PropertyName', propertyvalue, ...)
            %
            % PROPERTIES
            %     see <a href="matlab:help ini.utils.edit">ini.utils.edit</a>

            info = self.Rule.Info;
            if isempty(info)
                info = self.Name;
            end
            
            type = self.Rule.Type;
            if isempty(type)
                if isempty(self.Value)
                    % by default, entries are char
                    type = 'char';
                else
                    type = class(self.Value);
                end
            end
            
            ini.utils.edit(...
                'Title', [], ...
                varargin{:}, ...
                'Label', info, ...
                'Tooltip', self.usage(), ...
                'Type', type, ...
                'Default', self.Value, ...
                'Set', @(value) self.Rule.assign(self, value));
        end
        
        function txt = info(self)
            % Retrieve info text.
            
            txt = self.Rule.Info;
        end
        
        function txt = usage(self)
            % Retrieve info text.
            
            txt = self.Rule.Usage;
        end
        
        function self = write(self, fid, varargin)
            % Write key to file.
            
            [args, ~] = +ini.pvargs([], struct('Comments', false, 'SaveEmpty', false, 'Indent', '', 'first_line', false), varargin{:}); %#ok<RHSFN> 
        
            if self.isUnassigned() && ~args.SaveEmpty
                self.markSaved();
                return
            end
            
            if ~isempty(self.Parent) && self.Parent.Node == ini.node.SECTION && strcmp(self.Parent.Name, 'global')
                args.Indent = '';
            end

            value = ini.utils.string(self.Rule.retrieve(self, 'Reason', ini.reason.SAVE).Value);
            if args.Comments
                fprintf(fid, '%-40s ; %s\n', sprintf('%s%s = %s', args.Indent, self.Name, value), self.Rule.Info);
            else
                fprintf(fid, '%s%s = %s\n', args.Indent, self.Name, value);
            end
            
            self.markSaved();
        end
    end
    
end

