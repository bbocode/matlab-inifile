% Rule declaration utilites.
%
% SEE ALSO
%   <a href="matlab:help ini.rule">ini.rule</a>

RULESET = @(varargin) ini.rule(varargin{:}, 'Node', ini.node.RULESET); 
MAP     = @(varargin) ini.rule(varargin{:}, 'Node', ini.node.MAP); 
SECTION = @(varargin) ini.rule(varargin{:}, 'Node', ini.node.SECTION); 
KEY     = @(varargin) ini.rule(varargin{:}, 'Node', ini.node.KEY); 

