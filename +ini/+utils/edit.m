function edit(varargin)
% ini.utils.edit - Default edit function.
%
% The edit methods of ini.config to ini.key are designed so that is 
% possible to create a GUI for editing the configuration.
%
% USAGE
%   ini.utils.edit('PropertyName', property_value, ...)
%
% Properties:
%   Set <function>      The function to assign result of input.
%   Title <title>       Title to display.
%   Label <string>      Label of item to edit.
%   Tooltip <string>    Tooltip to display (only for GUIs).
%   Type, <type>        Input type: 
%                           logical         Yes/No question.
%                           double          Expect number (or array of numbers).
%                           char            A character string [default].
%   Default, <default>  Default answer.
%   Selection           List of existing answers (unhandled here).
%   Execute <boolean>   A boolean value of true means, that all edits
%                       have been set up (this is only used by ini.config).

    % parse args
    [args] = +ini.pvargs([], struct(...
        'Title', [], ...
        'Label', [], ...
        'Tooltip', [], ...
        'Type', 'char', ...
        'Default', [], ...
        'Set', [], ...
        'Selection', [], ...
        'Execute', true), varargin{:});        
    
    if ~args.Execute
        return
    end
    
    if ~isempty(args.Title)
        fprintf(1, '%s\n', args.Title)
    end
    
    if isempty(args.Default)
        switch args.Type
            case 'logical'
                args.Default = false;
            case 'double'
                args.Default = NaN;
            case 'char'
                args.Default = '';
        end
    end

    % input
    msg = args.Label;
    switch args.Type
        case 'logical'
            if args.Default
                msg = [msg ' [Y/n]'];
            else
                msg = [msg ' [y/N]'];
            end
            switch input([msg ': '], 's')
                case {'Y','y'}
                    res = true;
                case {'N','n'}
                    res = false;
                otherwise 
                    res = args.Default;
            end
        case 'double'
            if ~isnan(args.Default)
                msg = [msg ' [' num2str(args.Default(1)) ];
                for k = 2:length(args.Default)
                    msg = [ msg ',' num2str(args.Default(k)) ]; %#ok<AGROW>
                end
                msg = [msg ']'];
            end
            s = input([msg ': '], 's');
            if isempty(s)
                res = args.Default;
            else
                res = strsplit(strrep(s,',',' '));
                for k=1:length(res)
                    res{k} = str2double(res{k});
                end
                
                if length(res) == 1
                    res = res{1};
                end
            end

        case 'char'
            if ~isempty(args.Default)
                msg = [msg ' [' regexprep(args.Default, '[\\]', '\\\\') ']'];
            end
            res = input([msg ': '], 's');
            if isempty(res)
                res = args.Default;
            end
    end
    
    args.Set(res);
end

