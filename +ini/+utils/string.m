function res = string(obj)
    % Convert a value (char, double, logical) to a string representation.
    if ischar(obj)
        res = obj;
    else
        switch length(obj)
            case 0
                res = '';

            case 1
                res = tochar(obj);

            otherwise
                res = ['[ ' strjoin(arrayfun(@(v) tochar(v), obj, 'UniformOutput', false), ' ') ' ]'];
        end
    end
end
 
function s = tochar(val)
    switch class(val)
        case 'char'
            s = val;
        case 'double'
            s = sprintf('%g', val);
        case 'logical'
            if val
                s = 'true';
            else
                s = 'false';
            end
        otherwise
            s = class(val);
    end
end
