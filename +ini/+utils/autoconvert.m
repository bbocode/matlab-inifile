function B = autocast(A)
% Automatically transform string into a matlab type.
%
% FIXME
    try
        B = eval(A);
    catch me, %#ok<NASGU>
        B = A;
    end
    
    switch class(B)
        case {'logical', 'double', 'char'}
            % OK
        otherwise
            B = A;
    end
end

