function B = convert(A, T)
% Convert to a specific type.
%
% SEE ALSO 
% FIXME

    if isempty(T)
        B = ini.utils.autoconvert(A);
    else
        switch class(A)
            case {'char', 'string'}
                switch T
                    case 'char'
                        B = char(A);

                    case 'double'
                        if A(1) == '[' 
                            B = double(eval(A));
                        else
                            B = str2double(A);
                        end

                    case 'logical'
                        B = logical(eval(A));

                    otherwise
                        error('INI:utils:invalidConversion', 'Invalid type conversion (%s => %s).', class(A), class(T))
                end

            case 'logical'
                switch T
                    case 'char'
                        if length(A) > 1
                            B = sprintf('[%s]', strjoin(string(A)', ' '));
                        else
                            B = string(A);
                        end

                    case 'logical'
                        B = logical(A);

                    otherwise
                        error('INI:utils:invalidConversion', 'Invalid type conversion (%s => %s).', class(A), class(T))
                end

            case 'double'
                switch T
                    case 'char'
                        if length(A) > 1
                            B = sprintf('[%s]', strjoin(string(A)', ' '));
                        else
                            B = ini.utils.string(A);
                        end

                    case 'double'
                        B = double(A);

                    otherwise
                        error('INI:utils:invalidConversion', 'Invalid type conversion (%s => %s).', class(A), class(T))
                end


            otherwise
                error('INI:utils:invalidConversion', 'Invalid type conversion (%s => %s).', class(A), class(T))
        end
    end
end

