classdef pvargs < handle
    % Property-value argument parser.
    %
    % This property-value parser is implemented as a class to allow restricted
    % set access to class internal values (e.g. '(SetAccess = {?ini.pvargs})')
    %
    % USAGE
    %     args = ini.pvargs(object, varargin{:})
    %     args = ini.pvargs(object, property_list, varargin{:})
    %     args = ini.pvargs(object, property_definition, varargin{:})
    %
    % This parses the variable arguments (property-value pairs of the form 
    % { 'PropertyName', property_value, ... }) and assigns these to the 'object'. 
    % All 'unparsed' options (i.e. non-property-value pairs or properties that
    % are not allowed, see below) are stored internally in the property 'Unparsed'. 
    %
    % If a property list is given (cell array of strings), only this property
    % names are allowed and assigned.
    %
    % If a property definition is given (struct with property names and default 
    % values), those properties will be assigned.

    methods
        function self = pvargs(object, varargin)
            % Constructor.
        
            % object can be empty
            if isempty(object)
                object = struct();
            end
        
            % property names and arguments to parse
            switch class(varargin{1})
                case 'cell'
                    defaults = [];
                    property_names = varargin{1};
                    args = varargin(2:end);

                case 'struct'
                    defaults = varargin{1};
                    property_names = fieldnames(defaults);
                    args = varargin(2:end);

                otherwise
                    defaults = [];
                    property_names = fieldnames(object);
                    args = varargin;
            end

            % defaults
            if ~isempty(defaults)
                for n = 1:length(property_names)
                    object.(property_names{n}) = defaults.(property_names{n});
                end
            end

            unparsed = {};

            % parse arguments
            n = 1; len = length(args);
            while n <= len
                key = args{n};

                if ischar(key)
                    % current argument is a string and, thus, probably a property-
                    % value pair, if not matched, check case, return as 'unparsed'
                    if ~any(contains(property_names, key))
                        for p = 1:length(property_names)
                            if strcmpi(property_names{p}, key)
                                error('Properties are case sensitive (''%s'').', property_names{p})
                            end
                        end

                        unparsed = { unparsed{:}, args{n} }; %#ok<CCAT>
                        if n < length(args)
                            unparsed = { unparsed{:}, args{n+1} }; %#ok<CCAT>
                        end
                        n = n + 1;
                    else
                        if n == len
                            % it's the last argument, this cannot be a pv-pair
                            unparsed = [ unparsed, args{n} ]; %#ok<AGROW>
                        else
                            % assign property value
                            if isstruct(object)
                                object.(key) = args{n+1};
                            else
                                if isprop(object, key)
                                    object.(key) = args{n+1};
                                else
                                    unparsed = { unparsed{:}, args{n}, args{n+1} }; %#ok<CCAT>
                                end
                            end
                            n = n + 1;
                        end
                    end
                else
                    % current argument is not a string, return as 'unparsed'
                    unparsed = { unparsed{:}, args{n} }; %#ok<CCAT>
                end

                % iterate
                n = n + 1;
            end
            
            self.Object = object;
            self.Unparsed = unparsed;
            
            function res = contains(array, key)
                res = cellfun(@(v) strcmp(key, v), array);
            end
        end
    end
    
    properties
        Object          % The object (with parsed properties).
        Unparsed        % Unhandled properties or arguments.
    end
    
    methods(Hidden)
        function varargout = uplus(self)
            % De-referencing operator.
            %
            % USAGE
            %   object = +ini.pvargs(...);
            %   [object,unparsed] = +ini.pvargs(...);
            %
            % NOTE
            %   Matlab itself seems to not restrict uplus to return multiple values. Future versions may,
            %   however, detect the mis-use.
        
            varargout{1} = self.Object;
            if nargout > 1
            	varargout{2} = self.Unparsed;
            else
                if ~isempty(self.Unparsed)
                    error('INI:pvargs:unparsedArguments', 'There are unparsed arguments.')
                end
            end
        end
    end
end
