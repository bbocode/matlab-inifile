classdef rule < handle
    % INI-file rule(s).
    %
    % Config file formats may be defined by rules. These rules can be used
    % to define what sections and keys are allowed. Rules can also be used for
    % documentation purposes or online help on configuration values (such as
    % in the case of interactive editing of keys). 
    %
    % In addition, keys or sections may have callbacks. Theses callbacks can
    % be used to introduce other features such as read-only values (e.g. a
    % a config file version), or to translate the per se string-only INI format 
    % to Matlab types, which are not supported by this library.
    %
    % USAGE
    %    rule = ini.rule('PropertyName', propertyvalue, ...)
    %    rule = ini.rule(sub_rule, 'PropertyName', propertyvalue, ...)
    %
    % PROPERTIES
    %    Type            Value type ('section' [default], 'char', 'logical', 'double')
    %    Subsection      Section has sub-sections (logical, default: false).
    %    Info            Variable or section info.
    %    Usage           Full variable or section help text.
    %    Default         Default value (property name can be omitted).
    %    Callback        Callback or list of callbacks (see below).
    %
    % CALLBACKS
    % Callbacks allow the implementation of such things as read-only attributes.
    %
    %     function value = callback('PropertyName', property_value, ...)
    %         [args, extra_args] = +ini.pvargs([], struct(...
    %             'Reason', ini.reason.UNDEFINED ...
    %         ), varargin{:});
    %           
    %         switch args.Reason
    %             case { ini.reason.INIT, ini.reason.RESET, ini.reason.GET, ini.reason.SET, ini.reason.LOAD, ini.reason.SAVE }
    %      
    %             otherwise
    %                 error('Internal error: unhandled call ''Reason'' (%s).', args.Reason);
    %         end
    %     end
    %
    % EXAMPLE
    % 
    %     ini.utils.rule;
    %     rules = RULESET(...
    %         SECTION(...
    %             'global', ...
    %                 'Info', 'Global settings', ...
    %                 KEY('version', 'Info', 'Config file version', 'Default', '0.5'), ...
    %                 KEY('owner',   'Type', 'char', 'Info', 'File owner') ...
    %             ), ...
    %         SECTION(...
    %             'core', ...
    %                 'Info', 'Core settings.', ...
    %                 KEY('editor',    'Type', 'char', 'Info', 'External editor'), ...
    %                 KEY('workspace', 'Type', 'char', 'Info', 'Current workspace', 'Callback', @(varargin) my_workspace_callback(varargin{:})) ...
    %             ), ...
    %         MAP(...
    %             'host', ...
    %                 'Info', 'Remote host settings.', ...
    %                 'Usage', 'Remote host settings should be defined when using repositories that are maintained by the user.', ...
    %                 KEY('user',   'Type', 'char', 'Info', 'Default user name (login) for host.'), ...
    %                 KEY('scheme', 'Type', 'char', 'Info', 'Preferred scheme (usually ssh, http or https [defualt: ssh])', 'Default', 'ssh') ...
    %             ), ...
    %         []);
    %
    % SEE ALSO
    %   <a href="matlab:help ini.utils.rule">ini.utils.rule</a>
    %   <a href="matlab:help ini.pvargs">ini.pvargs</a>
    %   <a href="matlab:help INI">INI</a>
    
    %%% Constructor
    methods
        function self = rule(varargin)
        % Constructor.
        
            % empty (default) rule
            if nargin == 0
                return
            end
 
            %%% parse arguments
            
            % first argument
            if isa(varargin{1}, 'ini.rule')
                varargin{1}.Parent = self;
                self.Children = { varargin{1} }; %#ok<CCAT1>
                self.Type = 'set';
            else
                % section or property rule
                self.Tag = varargin{1};        
            end
        
            % children
            n = 2;
            while n <= nargin
                if isa(varargin{n}, 'ini.rule')
                    % add child
                    varargin{n}.Parent = self;
                    self.Children{end+1} = varargin{n};
                elseif ~isempty(varargin{n})
                    % it's a property
                    if ~isprop(self, varargin{n})
                        error('Invalid or unknown property ''%s''.', string(varargin{n}))
                    else
                        % a property
                        self.(varargin{n}) = varargin{n+1};
                        n = n+1;
                    end
                else
                    % ignore
                end
                
                n = n+1;
            end
            
            % set Type property, if not provided, based on value
            if (self.Node == ini.node.KEY) && isempty(self.Type)
                if isempty(self.Default)
                    error('Invalid arguments: Property ''Type'' missing.');
                end
                
                self.Type = class(self.Default);
            end
            
            %%% sanity checks
            if ~isempty(self.Callback)
                if ~iscell(self.Callback)
                    self.Callback = { self.Callback };
                end
                
                for k = 1:numel(self.Callback)
                    if ~isa(self.Callback{k}, 'function_handle')
                        error('Internal error: expected a function handle.')
                    end
                end
            end
            
            %%% hand callbacks down
            for k = 1:length(self.Children)
                child = self.Children{k};
                child.Callback = { self.Callback{:}, child.Callback{:} }; %#ok<CCAT>
            end
        end
    end

    %%% Properties
    properties
        Tag                         % Key or section tag.
        
        Type = []                   % Value type.
        Default = []                % Default value.
        
        Callback = {}               % Callback functions.
        
        Info = []                   % Variable or section info.
        Usage = []                  % Terse variable or section help text.
    end
    
    properties
        Node = ini.node.UNDEFINED   % Node type.
        
        Parent = []                 % Parent rule.
        Children = {}               % Sub-rules.
    end

    methods(Hidden)
        function res = isEmpty(self)
        % Check if empty rule.
        
            res = (self.Node == ini.node.UNDEFINED);
        end
        
        function res = isRuleset(self)
        % Check, if this is a set of rules.
        
            res = (self.Node == ini.node.RULESET);
        end
        
        function res = isSectionRule(self)
        % Check, if this is a section.
        
            res = (self.Node == ini.node.SECTION) || (self.Node == ini.node.MAP);
        end
        
        function res = isKeyRule(self)
        % Check if this is a property rule.
        
            res = (self.Node == ini.node.KEY);
        end
        
        function res = hasSubsections(self)
        % Check, if this is a set of rules.
        
            res = (self.Node == ini.node.MAP);
        end
        
        function res = getRuleset(self)
        % Get root node.
            if self.isRuleset()
                res = self;
            elseif self.isSectionRule()
                res = self.Parent;
            else
                res = self.Parent.Parent;
            end
        end
        
        function res = getSection(self)
        % Get root node.
        
            if self.isRuleset()
                error('Internal error (trying to get section from rule-set).')
            elseif self.isSectionRule()
                res = self;
            else
                res = self.Parent;
            end
        end
        
        function res = isGlobalSectionRule(self)
        % Check, if this is the global section.
        
            res = self.isSectionRule() && strcmp(self.Tag, 'global');
        end
        
        function res = numRules(self)
            % Get number of children.
            res = length(self.Children);
        end
        
        function res = getRule(self, tag)
            % Get a child by index or name.
            if self.isEmpty()
                res = self;
                return
            end
            
            if isnumeric(tag)
                res = self.Children{tag};
            else
                res = [];
                for k=1:length(self.Children)
                    child = self.Children{k};
                    if strcmp(child.Tag, tag)
                        res = child;
                        return;
                    end
                end
                
                error('INI:rule:noSuchRule', 'Rule ''%s'' is not known.', tag);
            end
        end
        
        function emitCallbacks(self, varargin)
            % Emit callbacks.
            for i = 1:length(self.Callback)
                cb = self.Callback{i};
                cb(varargin{:});
            end
        end
        
        function item = retrieve(self, object, varargin)
            % Retrieve object.
            %
            % This method only passes back the object, but, in addition,
            % emits callbacks and possibly sets alternative default value.
            
            % parse arguments
            [args,~] = +ini.pvargs([], struct(...
                'Reason', ini.reason.GET, ...
                'Default', []), varargin{:});
            
            % if default value is given, update value
            if ~isempty(args.Default)
                if object.Node == ini.node.KEY
                    if isempty(object.Value)
                        self.assign(object, args.Default, 'Reason', args.Reason);
                    end
                else
                    error('INI:rule:invalidDefault', 'User supplied defaults are only allowed for keys.')
                end
            end
            
            % run callbacks
            self.emitCallbacks('Reason', args.Reason, 'Object', object, varargin{:});
            
            % make it so
            item = object;
        end
        
        function assign(self, object, value, varargin)
            % Assign object.
            %
            % This function assigns an object. Dependent on object type and
            % properties, it sets up the structure of ini.map, ini.section
            % or ini.key.
            
            args = +ini.pvargs([], struct(...
                'Reason', ini.reason.SET, ...
                'Default', [], ...              % ignored
                'Create', false ...             % ignored
            ), varargin{:});
            
            % emit callbacks (only if nothing will change)
            do_emit = false;
            switch object.Node
                case ini.node.KEY
                    if ~self.isEmpty() && (object.Node ~= self.Node)
                        error('INI:rule:invalidRule', 'Trying to apply %s rule to an object of type %s.', string(self.Node), string(object.Node))
                    end

                    cur_value = object.Value;
                    nxt_value = ini.utils.convert(value, self.Type);
                    
                    if isempty(cur_value) && isempty(nxt_value)
                        do_emit = false;
                    elseif isempty(cur_value) ~= isempty(nxt_value)
                        do_emit = true;
                    elseif ischar(cur_value)
                        do_emit = ~strcmp(cur_value, nxt_value);
                    else
                        do_emit = any(cur_value ~= nxt_value);
                    end
                    
                otherwise
                    do_emit = true;
            end
                    
            if do_emit
                self.emitCallbacks('Reason', args.Reason, 'Object', object, 'Value', value, varargin{:});
            end
            
            % make it so
            switch object.Node
                case ini.node.KEY
                    if ~self.isEmpty() && (object.Node ~= self.Node)
                        error('INI:rule:invalidRule', 'Trying to apply %s rule to an object of type %s.', string(self.Node), string(object.Node))
                    end

                    object.Value = ini.utils.convert(value, self.Type);
                    
                case ini.node.SECTION
                    if ~isstruct(value) && ~isempty(value)
                        error('INI:rule:expectedStruct', 'Value must be formatted as struct.');
                    end
                    
                    if ~self.isEmpty() 
                        if (self.Node ~= ini.node.SECTION) && (self.Node ~= ini.node.MAP)
                            error('INI:rule:invalidRule', 'Trying to apply %s rule to an object of type %s.', string(self.Node), string(object.Node))
                        end
                    end
                    
                    if ~isempty(value)
                        fn = fieldnames(value);
                        for k = 1:length(fn)
                            object.set(value.(fn{k}), 'Key', fn{k}, 'Reason', args.Reason);
                        end
                    end
                    
                case ini.node.MAP
                    if ~iscell(value) && ~isempty(value)
                        error('INI:rule:expectedCell', 'Value must be formatted as cell.');
                    end
                    
                    if ~self.isEmpty() && (object.Node ~= self.Node)
                        error('INI:rule:invalidRule', 'Trying to apply %s rule to an object of type %s.', string(self.Node), string(object.Node))
                    end

                    if ~isempty(value)
                        for k = 1:2:length(value)
                            object.set(value{k+1}, 'SubSection', value{k}, 'Reason', args.Reason);
                        end
                    end
                    
                case ini.node.CONFIG
                    if ~self.isEmpty() && (object.Node ~= self.Node)
                        error('INI:rule:invalidRule', 'Trying to apply %s rule to an object of type %s.', string(self.Node), string(object.Node))
                    end

                    error('FIXME')
                    
                otherwise
                    error('INI:rule:invalidNodeType', 'Trying to assign invalid node type (''%s'').', string(object.Node));
            end
            
            if (args.Reason == ini.reason.INIT) || (args.Reason == ini.reason.LOAD)
                object.markSaved();
            end
            
            if object.isModified()
                ptr = object;
                while ~isempty(ptr)
                    if ismethod(ptr, 'signalModified')
                        ptr.signalModified();
                    end
                    
                    if isprop(ptr, 'Parent')
                        ptr = ptr.Parent;
                    else
                        ptr = [];
                    end
                end
            end
        end
    end
    
    methods
        function disp(self)
            % Output annotated rule.
            if self.isEmpty()
                fprintf(1, 'ini.rule\n');
                return
            end
            
            if self.isRuleset()
                % loop over Children
                for k = 1:length(self.Children)
                    if k > 1
                        fprintf(1, '\n');
                    end
                    self.Children{k}.disp();
                end
            elseif self.isSectionRule()
                if ~isempty(self.Info)
                    fprintf(1, '### %s settings\n', sprintf(self.Info));
                end

                if ~isempty(self.Usage)
                    buf = strsplit(self.Usage, '\n');
                    for k = 1:length(buf)
                        fprintf(1, '%s\n', sprintf(buf{k}));
                    end
                end
            
                if self.isGlobalSectionRule()
                elseif self.hasSubsections()
                    fprintf(1, '[%s "..."]\n', self.Tag);
                else
                    fprintf(1, '[%s]\n', self.Tag);
                end
                
                for k = 1:length(self.Children)
                    self.Children{k}.disp();
                end
            else
                value = [self.Tag ' '];
                
                if ~isempty(self.Default)
                    switch self.Type
                        case 'char'
                            value = [ value '= ' self.Default];
                        case 'logical'
                            if self.Default
                                value = [ value '= true'];
                            else
                                value = [ value '= false'];
                            end
                        case 'double'
                            value = [ value '= ' num2str(self.Default)];
                        otherwise 
                            error('Internal error: invalid type.')
                    end
                end

                info = ''; %#ok<*AGROW>
                if ~isempty(self.Info)
                    info = [ info self.Info ' ' ];
                end
                
                if ~isempty(info)
                    fprintf(1, '%-40s; %s\n', value, info);
                else
                    fprintf(1, '%s\n', value);
                end
            end
        end
    end
end

