classdef map < handle
    % Representation of map of INI sub-sections.
    %
    % SEE ALSO
    %   <a href="matlab:help ini.section">ini.section</a>
    %   <a href="matlab:help INI">INI</a>
    
    properties(SetAccess={?ini.pvargs,?ini.rule})
        Name                % Name of INI section.
    end

    properties(Hidden, SetAccess={?ini.pvargs,?ini.rule})
        Sections            % Child container.
        Parent              % Parent section.
        Rule                % Rule for this section.
    end
    
    properties(Constant,Hidden)
        Node = ini.node.MAP
    end

    methods
        function self = map(varargin)
            % Constructor.

            self.Sections = containers.Map;
            
            % parse arguments
            [~,value] = +ini.pvargs(self, struct(...
                'Name', [], ...
                'Rule', ini.rule, ...
                'Parent', []), varargin{:});
            [value] = +ini.pvargs([], struct(...
                'Value', self.Rule.Default), value{:});
            
            if self.Rule.isEmpty()
                if isempty(self.Name)
                    error('INI:nameRequired', 'Sections or keys require a name (''Name'' property).');
                end
            else
                if ~isempty(self.Name) && ~strcmp(self.Name, self.Rule.Tag)
                    error('INI:nameOverdefined', 'Property ''Name'' over-defines section or key (as it is provided by the Rule).');
                end
                
                self.Name = self.Rule.Tag;
            end
            
            % initialize
            self.Rule.assign(self, value.Value, 'Reason', ini.reason.INIT);
        end
        
        function res = uplus(self)
            % De-reference values (return Matlab struct of configuration settings).
            
            self.Rule.retrieve(self); % emit callbacks
            
            res = {};
            
            keys = self.Sections.keys; 
            secs = self.Sections.values;
            
            for k = 1:length(keys)
                res = { res{:}, keys{k}, +secs{k} }; %#ok<CCAT>
            end
        end
    end    
    
    methods
        function res = isUnassigned(self)
            % Check, if section is empty.
            sec = self.Sections.values;
            
            res = isempty(sec);
            if res, return; end
            
            for k = 1:length(sec)
                res = sec{k}.isUnassigned();
                if ~res, return; end
            end
        end
        
        function res = isModified(self)
            % Check if property value has changed.
            sec = self.Sections.values;
            
            res = false;
            if ~isempty(sec)
                for k = 1:length(sec)
                    res = sec{k}.isModified();
                    if res, return; end
                end
            end
        end

        function self = markSaved(self)
            % Set section to unmodified state.
            sec = self.Sections.values;
            for k = 1:length(sec)
                sec{k}.markSaved();
            end
        end
        
        function res = get(self, varargin)
            % Get value(s).
            %
            % USAGE
            %     result = object.get('PropertyName', propertyvalue, ...)
            %
            % PROPERTIES
            %     SubSection <name>             Name of sub-section.
            %     Key <keyname>                 Name of key.
            %     Default <value>               Expected default value (if sub-section and key is unassigned).

            args = +ini.pvargs([], struct(...
                'SubSection', [], ...
                'Key', [], ...
                'Reason', ini.reason.SET, ...
                'Create', false, ...
                'Default', [] ...
            ), varargin{:});
        
            args.Create = args.Create | (args.Reason == ini.reason.LOAD) | (args.Reason == ini.reason.INIT);
        
            if isempty(args.Key)
                if ~isempty(args.SubSection)
                    if ~self.Sections.isKey(args.SubSection)
                        if args.Create == false
                            error('INI:section:noSuchSection', 'Section ''%s'' does not contain sub-section ''%s''.', self.Name, args.SubSection)
                        end
                        
                        self.Sections(args.SubSection) = ...
                            ini.section('Name', self.Name, 'SubSection', args.SubSection, ...
                                'Rule', self.Rule, 'Parent', self);
                    end
                    
                    res = self.Sections(args.SubSection);
                else
                    res = self;
                end
            else
                if ~self.Sections.isKey(args.SubSection)
                    if isempty(args.Default) || ~self.Rule.isEmpty() || (args.Create == false)
                        error('INI:section:noSuchSection', 'Section ''%s'' does not contain sub-section''%s''.', self.Name, args.SubSection)
                    end

                    % auto-create sub-section
                    if isstruct(args.Default)
                        self.Sections(args.SubSection) = ...
                            ini.section('Name', self.Name, 'SubSection', args.SubSection, ...
                                'Rule', self.Rule, 'Parent', self, 'Value', args.Default);
                    else
                        self.Sections(args.SubSection) = ...
                            ini.section('Name', self.Name, 'SubSection', args.SubSection, ...
                                'Rule', self.Rule, 'Parent', self);
                    end
                end
                sec = self.Sections(args.SubSection);
                
                if ~isfield(sec.Keys, args.Key)
                    if isempty(args.Default) || ~self.Rule.isEmpty() || (args.Create == false)
                        error('INI:section:noSuchKey', 'Section ''%s'' does not contain key ''%s''.', self.Name, args.Key)
                    end
                    
                    sec.Keys.(args.Key) = ini.key('Name', args.Key, 'Rule', self.Rule.getRule(args.Key), 'Parent', sec, 'Value', args.Default);
                end
                
                % assign default value if not assigned
                if sec.Keys.(args.Key).isUnassigned && ~isempty(args.Default)
                    sec.Keys.(args.Key).set(args.Default);
                end
                
                res = self.Rule.retrieve(sec.Keys.(args.Key), varargin{:});
            end
        end
        
        function self = set(self, value, varargin)
            % Set value.
            %
            % In general, 'value' follows the <a href="matlab:help INI">MAPPING BETWEEN MATLAB AND INI</a>
            % rules. If a single key should be assigned, property ''Key'' must be given.
            %
            % USAGE
            %     object.set(value, 'PropertyName', propertyvalue, ...)
            %
            % PROPERTIES
            %     SubSection <name>             Name of sub-section.
            %     Key <keyname>                 Assign single key.
            %     Reason <reason>               The reason for set operation (see ini.reason).

            args = +ini.pvargs([], struct(...
                'SubSection', [], ...
                'Key', [], ...
                'Reason', ini.reason.SET, ...
                'Create', false, ...
                'Default', [] ...  % ignored
            ), varargin{:});
        
            args.Create = args.Create | (args.Reason == ini.reason.LOAD) | (args.Reason == ini.reason.INIT);
            
            if isempty(args.SubSection)
                if ~isempty(args.Key)
                    error('INI:map:unexpectedKey', 'Unexpected property ''Key'' as ''SubSection'' is not provided.')
                end
                
                self.Rule.assign(self, value, 'Reason', args.Reason);
            else
                if ~isempty(args.Key)
                    if ~self.Sections.isKey(args.SubSection)
                        if ~self.Rule.isEmpty() || (args.Create == false)
                            error('INI:section:noSuchSubSection', 'Section ''%s'' does not contain sub-section''%s''.', self.Name, args.SubSection)
                        end

                        % auto-create sub-section
                        self.Sections(args.SubSection) = ...
                            ini.section('Name', self.Name, 'SubSection', args.SubSection, ...
                                'Rule', self.Rule.getRule(args.Key), 'Parent', self);
                    end
                    
                    sec = self.Sections(args.SubSection);
                    sec.set(value, 'Key', args.Key, 'Reason', args.Reason, 'Create', args.Create);
                else
                    if ~self.Sections.isKey(args.SubSection)
                        % auto-create sub-section
                        self.Sections(args.SubSection) = ...
                            ini.section('Name', self.Name, 'SubSection', args.SubSection, ...
                                'Rule', self.Rule, 'Parent', self);
                    end
                    sec = self.Sections(args.SubSection);
                    sec.set(value, 'Reason', args.Reason, 'Create', args.Create);
                end
            end
        end
        
        function self = reset(self)
            % Reset map content.
            
            self.Rule.assign(self, [], 'Reason', ini.reason.RESET);
            self.Sections = containers.Map;
        end
        
        function self = edit(self, varargin)
            % Edit value.
            %
            % USAGE
            %     object.edit('PropertyName', propertyvalue, ...)
            %
            % PROPERTIES
            %     SubSection <name>         Edit a specific sub-section.
            %     see <a href="matlab:help ini.utils.edit">ini.utils.edit</a>
            
            [args, edit_args] = +ini.pvargs([], struct('SubSection', []), varargin{:}); %#ok<RHSFN>
            
            label = self.Rule.Info;
            if isempty(label)
                label = self.Name;
            end
            
            if isempty(args.SubSection)
                ini.utils.edit(...
                    'Title', [ label ' settings' ], ...
                    varargin{:}, ...
                    'Label', label, ...
                    'Tooltip', self.usage(), ...
                    'Type', 'char', ...
                    'Default', [], ...
                    'Selection', [], ... % FIXME self.Sections.keys, no cell otherwise conversion to struct(1 x n)
                    'Set', @(subsection) self.edit_selected_subsection(subsection, varargin{:}));
            else
                sec = self.get(varargin{:});
                sec.edit(edit_args{:});
            end
        end
        
        function self = edit_selected_subsection(self, subsection, varargin)
            % INTENRAL: this method is called from
            if isempty(subsection)
                return
            end
            
            sec = self.get('SubSection', subsection, 'Create', true);
            sec.edit(varargin{:}, 'Title', []);
        end
        
        function txt = info(self)
            % Retrieve info text.
            
            txt = self.Rule.Info;
        end
        
        function txt = usage(self)
            % Retrieve info text.
            
            txt = self.Rule.Usage;
        end
        
        function self = write(self, fid, varargin)
            % Internal method: write map.
            
            [args, ~] = +ini.pvargs([], struct('Comments', false, 'SaveEmpty', false, 'Indent', '', 'first_line', false), varargin{:}); %#ok<RHSFN> 

            sec = self.Sections.values;
            for k = 1:length(sec)
                sec{k}.write(fid, varargin{:}, 'first_line', false);
                args.first_line = false;
            end
        end
    end
end
