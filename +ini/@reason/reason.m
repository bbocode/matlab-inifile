classdef reason
    % Enumeration of callback reason.
    %
    %
    
    enumeration
        UNDEFINED,
        
        % Initialization phase.
        INIT,
        % Configuration value reset.
        RESET,
        % Value retrieval.
        GET,
        % Value assignment.
        SET,
        % Value loaded from file (this allows filtering).
        LOAD,
        % Value saved to file (this allows filtering).
        SAVE,
    end
end

