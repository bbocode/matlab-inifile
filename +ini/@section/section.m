classdef section < handle
    % Representation of an INI section.
    %
    % Keys may (but need not to) be grouped into arbitrarily named 
    % <a href="https://en.wikipedia.org/wiki/INI_file#Sections">sections</a>. 
    % 
    % In addition, git-alike sub-sections 
    %   
    %   [section "sub-section"]
    %
    % are supported.
    %
    % See also <a href="matlab:help INI">MAPPING BETWEEN MATLAB AND INI</a>.
    %
    % USAGE
    %    object = ini.section('PropertyName', propertyvalue, ...)
    %
    % PROPERTIES
    %   Name <name>                 Associate a name with the key (required).
    %   SubSection <subsection>     Sub-section name.
    %   Data <initdata>             Initial key-value pairs (struct with key-value pairs).
    %   Rule <rule>                 A rule defining type etal. (see ini.rule).
    %
    % NOTE
    %   Section and key names cannot overlap with standard (handle) class methods. 
    %   Among these are: delete, FIXME
    %
    % SEE ALSO
    %   <a href="matlab:help INI">INI</a>
    
    properties(SetAccess={?ini.pvargs,?ini.rule})
        Name                % Name of INI section.
        SubSection = ''     % Sub-section name or maker as sub-section container.
    end

    properties(Hidden, SetAccess={?ini.pvargs,?ini.rule})
        Keys                % Child container.
        Parent              % Parent section.
        Rule                % Rule for this section.
    end
    
    properties(Constant,Hidden)
        Node = ini.node.SECTION
    end

    methods
        function self = section(varargin)
            % Constructor.

            self.Keys = struct();
            
            % parse arguments
            [~,value] = +ini.pvargs(self, struct(...
                'Name', [], ...
                'SubSection', [], ...
                'Rule', ini.rule, ...
                'Parent', []), varargin{:});
            [value] = +ini.pvargs([], struct(...
                'Value', self.Rule.Default), value{:});
            
            if self.Rule.isEmpty()
                if isempty(self.Name)
                    error('INI:nameRequired', 'Sections or keys require a name (''Name'' property).');
                end
            else
                if ~isempty(self.Name) && ~strcmp(self.Name, self.Rule.Tag)
                    error('INI:nameOverdefined', 'Property ''Name'' over-defines section or key (as it is provided by the Rule).');
                end
                
                self.Name = self.Rule.Tag;
            end
            
            % initialize
            for k = 1:self.Rule.numRules()
                key =  ini.key('Rule', self.Rule.getRule(k), 'Parent', self);
                self.Keys.(key.Name) = key;
            end
                
            self.Rule.assign(self, value.Value, 'Reason', ini.reason.INIT);
        end
        
        function res = uplus(self)
            % De-reference values (return Matlab struct of configuration settings).
            
            self.Rule.retrieve(self); % this emits callbacks
            
            res = struct();

            fn = fieldnames(self.Keys);
            for k = 1:length(fn)
                key = fn{k};
                child = self.Keys.(key); 
                res.(key) = +child;
            end
        end
    end

    methods
        function res = isUnassigned(self)
            % Check, if section is empty.
            fn = fieldnames(self.Keys);

            res = isempty(fn);
            if ~res
                for k = 1:length(fn)
                    res = self.Keys.(fn{k}).isUnassigned();
                    if ~res, return; end
                end
            end
        end
        
        function res = isModified(self)
            % Check if property value has changed.
            res = false;
            
            fn = fieldnames(self.Keys);
            for k = 1:length(fn)
                child = self.Keys.(fn{k});
                res = child.isModified();
                if res
                    break
                end
            end
        end

        function self = markSaved(self)
            % Set section to unmodified state.
            fn = fieldnames(self.Keys);
            for k = 1:length(fn)
                child = self.Keys.(fn{k});
                child.markSaved();
            end
        end
        
        function res = get(self, varargin)
            % Get value(s).
            %
            % USAGE
            %     result = object.get('PropertyName', propertyvalue, ...)
            %
            % PROPERTIES
            %     Key <keyname>                 Name of key.
            %     Default <value>               Expected default value (if section or key is unassigned).
            
            args = +ini.pvargs([], struct(...
                'Key', [], ...
                'Reason', ini.reason.SET, ...
                'Create', false, ...
                'SubSection', [], ...
                'Default', [] ...
            ), varargin{:});
        
            if ~isempty(args.SubSection)
                error('INI:section:noSubSections', 'This section (''%s'') does not have sub-sections.', self.Name);
            end
        
            args.Create = args.Create | (args.Reason == ini.reason.LOAD) | (args.Reason == ini.reason.INIT);
        
            if isempty(args.Key)
                res = self;
            else
                % create key if it does not exists
                if ~isfield(self.Keys, args.Key)
                    if isempty(args.Default) || ~self.Rule.isEmpty() || (args.Create == false)
                        error('INI:section:noSuchKey', 'Section ''%s'' does not contain key ''%s''.', self.Name, args.Key)
                    end
                    
                    self.Keys.(args.Key) = ini.key('Name', args.Key, 'Rule', self.Rule.getRule(args.Key), 'Parent', self, 'Value', args.Default);
                end
                
                % assign default value if not assigned
                if self.Keys.(args.Key).isUnassigned && ~isempty(args.Default)
                    self.Keys.(args.Key).set(args.Default);
                end
                
                % return key
                res = self.Rule.retrieve(self.Keys.(args.Key), varargin{:});
            end
        end
        
        function self = set(self, value, varargin)
            % Set value.
            %
            % In general, 'value' follows the <a href="matlab:help INI">MAPPING BETWEEN MATLAB AND INI</a>
            % rules. If a single key should be assigned, property ''Key'' must be given.
            %
            % USAGE
            %     object.set(value, 'PropertyName', propertyvalue, ...)
            %
            % PROPERTIES
            %     Key <keyname>                 Assign single key.
            %     Reason <reason>               The reason for set operation (see ini.reason).

            args = +ini.pvargs([], struct(...
                'Key', [], ...
                'Reason', ini.reason.SET, ...
                'Create', false, ...
                'SubSection', [], ...
                'Default', [] ...  % ignored
            ), varargin{:});
        
            if ~isempty(args.SubSection)
                error('INI:section:noSubSections', 'This section (''%s'') does not have sub-sections.', self.Name);
            end
        
            args.Create = args.Create | (args.Reason == ini.reason.LOAD) | (args.Reason == ini.reason.INIT);
            
            if isempty(args.Key)
                self.Rule.assign(self, value, 'Reason', args.Reason);                
            else
                if ~isfield(self.Keys, args.Key)
                    if ~self.Rule.isEmpty() || (args.Create == false)
                        error('INI:section:noSuchKey', 'Section ''%s'' does not contain key ''%s''.', self.Name, args.Key)
                    end
                    
                    self.Keys.(args.Key) = ini.key('Name', args.Key, 'Rule', self.Rule.getRule(args.Key), 'Parent', self);
                end
                
                self.Keys.(args.Key).set(value, 'Reason', args.Reason);
            end
        end
        
        function self = reset(self)
            % Reset to default.
            %
            % USAGE
            %     object.reset()
            
            self.Rule.assign(self, [], 'Reason', ini.reason.RESET);

            fn = fieldnames(self.Keys);
            for k = 1:length(fn)
                key = fn{k};
                child = self.Keys.(key); 
                child.reset();
            end
        end
        
        function self = edit(self, varargin)
            % Edit value.
            %
            % USAGE
            %     object.edit('PropertyName', propertyvalue, ...)
            %
            % PROPERTIES
            %     see <a href="matlab:help ini.utils.edit">ini.utils.edit</a>

            title = self.Rule.Info;
            if isempty(title)
                title = self.Name;
            end
            
            if ~isempty(self.SubSection)
                title = [ title ' "' self.SubSection '"'];
            end
            
            fn = fieldnames(self.Keys);
            for k = 1:length(fn)
                key = fn{k};
                child = self.Keys.(key); 
                child.edit('Title', title, varargin{:});
                title = [];
            end
        end
        
        function txt = info(self)
            % Retrieve info text.
            
            txt = self.Rule.Info;
        end
        
        function txt = usage(self)
            % Retrieve info text.
            
            txt = self.Rule.Usage;
        end
        
        function self = write(self, fid, varargin)
            % Internal method: write section.

            [args, ~] = +ini.pvargs([], struct('Comments', false, 'SaveEmpty', false, 'Indent', '', 'first_line', false), varargin{:}); %#ok<RHSFN> 
        
            if self.isUnassigned() && ~args.SaveEmpty
                self.markSaved();
                return
            end

            self.Rule.retrieve(self, 'Reason', ini.reason.SAVE);
            
            if ~args.first_line
                fprintf(fid, '\n');
            end

            if args.Comments
                fprintf(fid, ';;; %s settings\n', self.Rule.Info);
            end
            
            if isempty(self.SubSection)
                if ~strcmp(self.Name, 'global')
                    fprintf(fid, '[%s]\n', self.Name);
                end
            else
                fprintf(fid, '[%s "%s"]\n', self.Name, self.SubSection);
            end
            
            fn = fieldnames(self.Keys);
            for k = 1:length(fn)
                child = self.Keys.(fn{k}); 
                child.write(fid, varargin{:}, 'first_line', false);
            end

            self.markSaved();
        end
    end
end

