classdef INI < handle
    % INI file handler.
    %
    % This handler mostly follows the <a href="https://en.wikipedia.org/wiki/INI_file">INI file (Wikipedia)</a>
    % format. In addition, it supports sub-sections ([section "sub-section"]) as, for example, used by git.
    % Also, arrays of numbers are supported.
    %
    % USAGE
    %   config = INI('Property', propertyvalue, ...);
    %
    %   
    %
    % PROPERTIES
    %   File                    Configuration file.
    %   AutoSave                Save any change immediately (default: false).
    %   Rules                   Supply configuration rules (see <a href="matlab:help ini.rule">ini.rule</a>).
    %   Indent                  Indentation (default: '').
    %
    % EXAMPLE
    % See <a href="matlab:edit example_matlab_inifile">example_matlab_inifile</a>.
    %
    % SEE ALSO
    %   <a href="matlab:help ini.rule">ini.rule</a>
    %   <a href="matlab:help ini.map">ini.map</a>
    %   <a href="matlab:help ini.section">ini.section</a>
    %   <a href="matlab:help ini.key">ini.key</a>
    %   <a href="matlab:help ini.reason">ini.reason</a>
    %   <a href="matlab:help ini.node">ini.node</a>

    % Values are represented as 'char', 'double' (including arrays) or 'logical'.
    % Sections are represented as structure:
    %     struct('key', keyvalue, ...)
    % Sub-sections are represented as cells:
    %     { 'sub-section', struct('key', keyvalue, ...) }
    
    properties
        File            % Full path to INI file.
        AutoSave        % Save INI on change.
        Rules           % Rules.
        Indent          % Indent output.
    end

    properties
        Sections
    end
    
    methods
        function self = INI(varargin)
            % Constructor.
            
            %%% parser arguments
            [~] = +ini.pvargs(self, struct(...
                'File', [], ...
                'AutoSave', false, ...
                'Indent', '', ...
                'Rules', ini.rule() ...
            ), varargin{:});
            
            %%% load file
            self.resetSettings();
            self.importSettings();
        end

        function delete(self)
            % Destructor
            delete(self.Rules)
            fn = fieldnames(self.Sections);
            for k = 1:length(fn)
                delete(self.Sections.(fn{k}));
            end
        end
        
        function res = uplus(self)
            % Convert to matlab structure.
            
            res = struct();
            
            fn = fieldnames(self.Sections);
            for k = 1:length(fn)
                res.(fn{k}) = +self.Sections.(fn{k});
            end
        end
    end
    
    methods
        function [varargout] = subsref(self, S)
            switch S(1).type
                case '.'
                    if isprop(self, S(1).subs) || ismethod(self, S(1).subs)
                        [varargout{1:nargout}] = builtin('subsref', self, S);
                    else
                        if ~ischar(S(1).subs)
                            error('INI:invalidIndexing', 'Invalid index or reference.');
                        end
                        
                        if ~isfield(self.Sections, S(1).subs)
                            error('INI:noSuchSection', 'Section ''%s'' does not exist.', S(1).subs);
                        end
                        
                        if length(S) == 1
                            [varargout{1:nargout}] = self.Sections.(S(1).subs);
                        else
                            % get section(subsection).key
                            section = S(1).subs;
                            subsection = [];
                            key = [];
                            
                            % last part might be a function, e.g. .edit()
                            last = length(S);
                            for k = 3:last
                                switch S(k).type
                                    case '()'
                                        last = k-2;
                                        break;
                                end
                            end
                            
                            % parse S
                            if last >= 2
                                switch S(2).type
                                    case '.'
                                        key = S(2).subs;

                                    case '()'
                                        subsection = S(2).subs{1};
                                        % FIXME sanity checks

                                        if last >= 3
                                            switch S(3).type
                                                case '.'
                                                    key = S(3).subs;

                                                otherwise
                                                    error('INI:invalidIndexing', 'Invalid index or reference.');
                                            end
                                        end

                                    otherwise
                                        error('INI:invalidIndexing', 'Invalid index or reference.');
                                end
                            end
                            
                            % index it
                            s = self.Sections.(section);
                            res = s.get('Key', key, 'SubSection', subsection);
                            
                            if last == length(S)
                                [varargout{1:nargout}] = res;
                            else
                                [varargout{1:nargout}] = builtin('subsref', res, S(last+1:end));
                            end
                        end
                    end
                    
                otherwise
                    error('INI:invalidIndexing', 'Invalid index or reference.');
            end
        end
        
        function self = subsasgn(self, S, value)
            switch S(1).type
                case '.'
                    if isprop(self, S(1).subs)
                        self = builtin('subsasgn', self, S, value);
                    else
                        if ~ischar(S(1).subs)
                            error('INI:invalidIndexing', 'Invalid index or reference.');
                        end

                        section = S(1).subs;
                        subsection = [];
                        key = [];

                        if length(S) > 1
                            switch S(2).type
                                case '.'
                                    if length(S) ~= 2
                                        error('INI:invalidIndexing', 'Invalid index or reference.');
                                    end

                                    key = S(2).subs;
                                case '()'
                                    subsection = S(2).subs;

                                    if length(S) == 3
                                        switch S(3).type
                                            case '.'
                                                key = S(3).subs;
                                            otherwise
                                                error('INI:invalidIndexing', 'Invalid index or reference.');
                                        end
                                    elseif length(S) > 3
                                        error('INI:invalidIndexing', 'Invalid index or reference.');
                                    end

                                otherwise
                                    error('INI:invalidIndexing', 'Invalid index or reference.');
                            end
                        end

                        if ~isfield(self.Sections, section)
                            if ~self.Rules.isEmpty()
                                error('INI:noSuchSection', 'Section ''%s'' does not exist.', S(1).subs);
                            end
                            
                            % auto-generate
                            if isempty(subsection)
                                self.Sections.(section) = ini.section('Name', section, 'Rule', self.Rules.getRule('global'), 'Parent', self);
                            else
                                self.Sections.(section) = ini.map('Name', section, 'Rule', self.Rules.getRule('global'), 'Parent', self);
                            end
                        end

                        % assign
                        s = self.Sections.(section);

                        s.set(value, 'Key', key, 'SubSection', subsection, 'Create', true);
                    end
                    
                otherwise
                    error('INI:invalidIndexing', 'Invalid index or reference.');
            end
        end
        
    end

    methods
        function signalModified(self)
            if self.AutoSave
                self.exportSettings();
            end
        end
    end
    
    methods
        function res = isModified(self)
            % Check if configuration is modified.
            
            res = false;
            fn = fieldnames(self.Sections);
            for k = 1:length(fn)
                sec = self.Sections.(fn{k});
                res = sec.isModified();
                if res, return; end
            end
        end
        
        function self = resetSettings(self)
            % Reset config.
            
            self.Sections = struct();
            if self.Rules.isEmpty()
                self.Sections.global = ini.section('Name', 'global', 'Rule', self.Rules.getRule('global'), 'Parent', self);
            else
                % make sure, this is the first entry
                try
                    rule = self.Rules.getRule('global');
                catch me,  %#ok<NASGU>
                    warning('INI:missingGlobalRule', 'Ruleset does not contain a global entry.');
                    rule = ini.rule();
                end
                self.Sections.global = ini.section('Name', 'global', 'Rule', rule, 'Parent', self);
                
                % create entries
                for k = 1:self.Rules.numRules()
                    rule = self.Rules.getRule(k);
                    
                    if rule.hasSubsections()
                        self.Sections.(rule.Tag) = ini.map('Rule', rule, 'Parent', self);
                    else
                        self.Sections.(rule.Tag) = ini.section('Rule', rule, 'Parent', self);
                    end
                end
            end
        end

        function self = importSettings(self)
            % (Re-)load INI file.
            
            %%% read INI file line-wise
            lines = {};
            fid = fopen(self.File);
            if fid < 0, return; end

            % read line by line
            while ~feof(fid)
                line = fgets(fid);
                if ~ischar(line), continue; end

                lines{end+1} = deblank(line); %#ok<AGROW>
            end

            fclose(fid);

            % skip trailing empty lines
            while ~isempty(lines) && isempty(lines{end})
                lines = lines(1:end-1); 
            end
        
            %%% parse lines
            section = 'global';
            subsection = [];
        
            % loop over all lines
            for l = 1:length(lines)
                line = lines{l};
                
                %%% comments start with a ';' in first column
                if isempty(line) || line(1) == ';'
                    continue;
                end
                
                % other comments
                s = strfind(line, ';');
                if ~isempty(s)
                    line = line(1:s-1);
                end

                %%% [section] or [section "sectionname"] ?
                s = regexp(line, '^[ \t]*\[(?<section>[^\]" ]*)[ ]*["]?(?<subsection>[^"]*)?["]?\][ \t]*$', 'names');
                if ~isempty(s)
                    %%% section
                    section = lower(s.section); % FIXME warn upper-case?
                    subsection = s.subsection;
                else
                    %%% key=value
                    s = strsplit(strtrim(line), '=');
                    if numel(s) < 2
                        error('Invalid file format.');
                    elseif numel(s) > 2
                        s{2} = strjoin(s(2:end), '=');
                    end

                    % create or modify content
                    if ~isfield(self.Sections, section)
                        if ~self.Rules.isEmpty()
                            error('INI:invalidFileOrRules', 'Section ''%s'' represented in file is not in the rule-set.', section);
                        end
                        
                        if isempty(subsection)
                            self.Sections.(section) = ini.section('Name', section, 'Rule', self.Rules.getRule(section), 'Parent', self);
                        else
                            self.Sections.(section) = ini.map('Name', section, 'Rule', self.Rules.getRule(section), 'Parent', self);
                        end
                    end
                        
                    % assign value
                    sec = self.Sections.(section);
                    
                    if sec.Node == ini.node.MAP
                        if isempty(subsection)
                            error('INI:expectedSubsection', 'Section ''%s'' requires a sub-section.', section)
                        end
                        
                        sec.set({ subsection, struct()});
                        sec = sec.get('SubSection', subsection);
                    else
                        if ~isempty(subsection)
                            error('INI:unexpectedSubsection', 'Section ''%s'' is not expected to have sub-sections.', section)
                        end
                    end
                    
                    keyname = lower(strtrim(s{1}));
                    
                    keyrule = sec.Rule.getRule(keyname);
                    if isempty(keyrule) || keyrule.isEmpty()
                        keyvalue = ini.utils.autoconvert(strtrim(s{2}));
                    else
                        keyvalue = ini.utils.convert(strtrim(s{2}), keyrule.Type);
                    end

                    sec.set(keyvalue, 'Key', keyname, 'Reason', ini.reason.LOAD);
                end
            end
        end
        
        function self = exportSettings(self, varargin)
            % Export config.
            
            fid = fopen(self.File, 'w');
            % FIXME check fid < 0
            self.write(fid, varargin{:}, 'Indent', self.Indent, 'first_line', true);
            fclose(fid);
        end
        
        function self = write(self, fid, varargin)
            % INTERNAL: write config to file descriptor
            [args, ~] = +ini.pvargs([], struct('Comments', false, 'SaveEmpty', false, 'Indent', '', 'first_line', false), varargin{:}); %#ok<RHSFN> 
            
            fn = fieldnames(self.Sections);
            for k = 1:length(fn)
                section = self.Sections.(fn{k});
                section.write(fid, varargin{:}, 'first_line', args.first_line);
                
                if ~section.isUnassigned() || args.SaveEmpty
                    args.first_line = false;
                end
            end
        end
    end
end
